from crispy_forms.bootstrap import TabHolder, Tab, FormActions
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Field, Submit, HTML
from django import forms
from book.models import Book, BookAuthor
from django.utils.translation import ugettext_lazy as _
from django.forms.widgets import CheckboxSelectMultiple
from .models import Author
from django_select2.forms import (
    HeavySelect2MultipleWidget, HeavySelect2Widget, ModelSelect2MultipleWidget,
    ModelSelect2TagWidget, ModelSelect2Widget, Select2MultipleWidget,
    Select2Widget
)
from django.forms.utils import flatatt
from django.utils.safestring import mark_safe

from django.forms.models import ModelChoiceIterator, ModelChoiceField
from django.forms.models import (ModelChoiceIterator, ModelMultipleChoiceField)
from functools import partial
from itertools import groupby
from operator import attrgetter


class Grouped(object):
    def __init__(self, queryset, group_by_field,
                 group_label=None, *args, **kwargs):
        """
        ``group_by_field`` is the name of a field on the model to use as
                           an optgroup.
        ``group_label`` is a function to return a label for each optgroup.
        """
        super(Grouped, self).__init__(queryset, *args, **kwargs)
        self.group_by_field = group_by_field
        if group_label is None:
            self.group_label = lambda group: group
        else:
            self.group_label = group_label

    def _get_choices(self):
        if hasattr(self, '_choices'):
            return self._choices
        return GroupedModelChoiceIterator(self)


class GroupedModelChoiceIterator(ModelChoiceIterator):
    def __iter__(self):
        if self.field.empty_label is not None:
            yield ("", self.field.empty_label)
        queryset = self.queryset.all()
        if not queryset._prefetch_related_lookups:
            queryset = queryset.iterator()
        for group, choices in groupby(self.queryset.all(),
                                      key=lambda row: getattr(row, self.field.group_by_field)):
            if self.field.group_label(group):
                yield (
                    self.field.group_label(group),
                    [self.choice(ch) for ch in choices]
                )


class GroupedModelChoiceField(Grouped, ModelChoiceField):
    choices = property(Grouped._get_choices, ModelChoiceField._set_choices)


class GroupedModelMultiChoiceField(Grouped, ModelMultipleChoiceField):
    choices = property(Grouped._get_choices, ModelMultipleChoiceField._set_choices)


class AuthorForm(forms.ModelForm):
    # books = GroupedModelMultiChoiceField(queryset=Book.objects.all(), group_by_field='name', widget=Select2MultipleWidget, required=False)
    books = ModelMultipleChoiceField(queryset=Book.objects.all(), widget=Select2MultipleWidget, required=False)

    def __init__(self, *args, **kwargs):
        super(AuthorForm, self).__init__(*args, **kwargs)
        self.fields['books'].initial = Book.objects.filter(authors__bookauthor__id_author=self.instance.id)
        print(Author.objects.filter(authors__bookauthor__id_author=self.instance.id))
        self.fields['name'].widget.attrs = {'class': 'form-control '}
        self.fields['surname'].widget.attrs = {'class': 'form-control '}


    class Meta:
        model = Author
        fields = '__all__'

