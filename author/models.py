from django.db import models
from django.utils.translation import pgettext
from django.urls import reverse
from django.db.models import Q

class Author(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=50, blank=True)
    surname = models.CharField(max_length=50, blank=True)

    def __str__(self):
        return '{0} {1}'.format(self.name, self.surname)

    class Meta:
        managed = True
        db_table = 'authors'