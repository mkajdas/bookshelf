# Author url

from django.urls import path, include
from . import views
from rest_framework import routers
from django.conf.urls import url
from django.urls import path, include
from . import views

urlpatterns = [
    path('author/', views.AuthorList.as_view(), name='author_list'),
    path('author/new', views.AuthorCreate.as_view(), name='author_new'),
    path('author/view/<int:pk>', views.AuthorView.as_view(), name='author_view'),
    path('author/edit/<int:pk>', views.AuthorUpdate.as_view(), name='author_edit'),
    path('author/delete/<int:pk>', views.AuthorDelete.as_view(), name='author_delete'),
]
