from __future__ import unicode_literals
from django.utils.translation import ugettext_lazy as _
from django.shortcuts import render
from django import forms
from .forms import AuthorForm
from django.views.generic.base import TemplateView
from django.views.generic import ListView, DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy
from .models import Author
from django.conf import settings


from django.http import HttpResponseRedirect

# List View
class AuthorList(ListView):
    model = Author
    # paginate_by = 10
    # context_object_name = 'users'  # Default: object_list
    template_name = 'author/author_list.html'  # Default: <app_label>/<model_name>_list.html
    queryset = Author.objects.all() #.select_related('id_book')
    # queryset = User.objects.all()  # Default: Model.objects.all()


# Detail View
class AuthorView(DetailView):
    model = Author

# Create View
class AuthorCreate(CreateView):
    model = Author
    form_class = AuthorForm
    # Setting returning URL
    success_url = reverse_lazy('author_list')

# Update View
class AuthorUpdate(UpdateView):
    model = Author
    form_class = AuthorForm
    success_url = reverse_lazy('author_list')    # Setting returning URL

    def form_valid(self, form):
        print("-------------------------------------")
        if settings.DEBUG:
            print("author: is valid form")
        print(form.cleaned_data)
        form.save()
        print("-------------------------------------")
        return HttpResponseRedirect(self.get_success_url())

    def form_invalid(self, form, **kwargs):
        print("=================================")
        if settings.DEBUG:
            print("author: is NOT valid form")
        print(form.errors)
        context = self.get_context_data(**kwargs)
        context['form'] = form
        print(form.cleaned_data)
        print("=================================")
        return self.render_to_response(context)


# Delete View
class AuthorDelete(DeleteView):
    model = Author
    # Setting returning URL
    success_url = reverse_lazy('author_list')

