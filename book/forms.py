from crispy_forms.bootstrap import TabHolder, Tab, FormActions
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Field, Submit, HTML
from django import forms
from django.utils.translation import ugettext_lazy as _
from django.forms.widgets import CheckboxSelectMultiple
from .models import Book, BookAuthor
from author.models import Author
from django_select2.forms import (
    HeavySelect2MultipleWidget, HeavySelect2Widget, ModelSelect2MultipleWidget,
    ModelSelect2TagWidget, ModelSelect2Widget, Select2MultipleWidget,
    Select2Widget
)
from itertools import groupby
from django.forms.models import (ModelChoiceIterator, ModelMultipleChoiceField)
from django.forms.utils import flatatt
from django.utils.safestring import mark_safe

class Grouped():
    def __init__(self, queryset, group_by_field, group_label=None, *args, **kwargs):
        self.q = queryset
        super(Grouped, self).__init__(queryset, *args, **kwargs)

    def _get_choices(self):
        collector = []
        item_qs = self.q
        for alphabet_letter, items in groupby(item_qs, lambda x: x.surname[0].upper()):
            surnames = []
            for ii in items:
                if ii.surname == '-':
                    o = '-'
                else:
                    o = ii.surname + ' ' + ii.name
                surnames.append ((ii.id, o))
            surnames_t = tuple(surnames)
            if alphabet_letter != '-':
                tup1 = (alphabet_letter, surnames_t)
            else:
                tup1 = ('', surnames_t)
            collector.append(tup1)
        t = tuple(collector)
        return t



class GroupedModelChoiceIterator(ModelChoiceIterator):
    def __iter__(self):
        if self.field.empty_label is not None:
            yield ("", self.field.empty_label)
        queryset = self.queryset.all()
        if not queryset._prefetch_related_lookups:
            queryset = queryset.iterator()
        for group, choices in groupby(self.queryset.all(),
                                      key=lambda row: getattr(row, self.field.group_by_field)):
            yield (
                self.field.group_label(group),
                [self.choice(ch) for ch in choices]
            )


class GroupedModelMultiChoiceField(Grouped, ModelMultipleChoiceField):
    choices = property(Grouped._get_choices, ModelMultipleChoiceField._set_choices)


class BookForm(forms.ModelForm):
    # authors = GroupedModelMultiChoiceField(queryset=Author.objects.all(), group_by_field='surname', widget=Select2MultipleWidget, required=False)
    authors = ModelMultipleChoiceField(queryset=Author.objects.all(), widget=Select2MultipleWidget, required=False)

    def __init__(self, *args, **kwargs):
        super(BookForm, self).__init__(*args, **kwargs)
        # print(Author.objects.filter(authors__bookauthor__id_author=self.instance.id))
        self.fields['authors'].initial = Author.objects.filter(authors__bookauthor__id_book=self.instance.id)
        self.fields['pages'].widget.attrs = {'class': 'form-control '}
        self.fields['name'].widget.attrs = {'class': 'form-control'}

    class Meta:
        model = Book
        fields = '__all__'
