from django.db import models
from decimal import Decimal
from django.utils.translation import pgettext


class Book(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=100, blank=True)
    pages = models.DecimalField(max_digits=10, decimal_places=0, blank=True, null=True)
    authors = models.ManyToManyField('author.Author', through='BookAuthor', through_fields=('id_book','id_author'), related_name='authors', blank=True)

    def __str__(self):
        return '{}'.format(self.name)

    def __unicode__(self):
        return '{}'.format(self.name)


    class Meta:
        managed = True
        db_table = 'books'



class BookAuthor(models.Model):
    id = models.AutoField(primary_key=True)
    id_book = models.ForeignKey(Book, models.CASCADE, db_column='id_book')
    id_author = models.ForeignKey('author.Author', models.CASCADE, db_column='id_author')


    class Meta:
        managed = True
        db_table = 'books_authors'

