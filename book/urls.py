# Book url

from django.urls import path, include
from . import views
from rest_framework import routers
from django.conf.urls import url
from django.urls import path, include
from . import views

urlpatterns = [
    path('book/', views.BookList.as_view(), name='book_list'),
    path('book/new', views.BookCreate.as_view(), name='book_new'),
    path('book/view/<int:pk>', views.BookView.as_view(), name='book_view'),
    path('book/edit/<int:pk>', views.BookUpdate.as_view(), name='book_edit'),
    path('book/delete/<int:pk>', views.BookDelete.as_view(), name='book_delete'),
]
