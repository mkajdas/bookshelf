from __future__ import unicode_literals
from django.utils.translation import ugettext_lazy as _
from django.shortcuts import render
from django import forms
from .forms import BookForm
from django.views.generic.base import TemplateView
from django.views.generic import ListView, DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy
from .models import Book
from django.conf import settings


from django.http import HttpResponseRedirect

# List View
class BookList(ListView):
    model = Book
    # paginate_by = 10
    # context_object_name = 'users'  # Default: object_list
    template_name = 'book/book_list.html'  # Default: <app_label>/<model_name>_list.html
    queryset = Book.objects.all() #select_related('author')
    # queryset = User.objects.all()  # Default: Model.objects.all()


# Detail View
class BookView(DetailView):
    model = Book

# Create View
class BookCreate(CreateView):
    model = Book
    form_class = BookForm
    # Setting returning URL
    success_url = reverse_lazy('book_list')

# Update View
class BookUpdate(UpdateView):
    model = Book
    form_class = BookForm
    success_url = reverse_lazy('book_list')    # Setting returning URL

    def form_valid(self, form):
        print("-------------------------------------")
        if settings.DEBUG:
            print("book: is valid form")
        print(form.cleaned_data)
        form.save()
        print("-------------------------------------")
        return HttpResponseRedirect(self.get_success_url())

    def form_invalid(self, form, **kwargs):
        print("=================================")
        if settings.DEBUG:
            print("book: is NOT valid form")
        print(form.errors)
        context = self.get_context_data(**kwargs)
        context['form'] = form
        print(form.cleaned_data)
        print("=================================")
        return self.render_to_response(context)


# Delete View
class BookDelete(DeleteView):
    model = Book
    # Setting returning URL
    success_url = reverse_lazy('book_list')

