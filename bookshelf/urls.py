"""
ezarzadcam2 URL Configuration
"""

from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from main.views import dashboard_view

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', dashboard_view, name='Main Page'),
    path('', include('book.urls')),
    path('', include('author.urls')),
]


if settings.DEBUG:
    import debug_toolbar
    urlpatterns = [path('__debug__/', include(debug_toolbar.urls)),] + urlpatterns