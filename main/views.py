from django.shortcuts import render
from django.http import HttpResponse
from django.apps import apps

def dashboard_view(request, *args, **kwargs):
    apps.get_models()
    return render(request, "main/base.html", {})
